import { Story, Meta } from '@storybook/react/types-6-0'
import Main from '.'

export default {
  title: 'Main',
  component: Main,
  args: {
    title: 'title default',
    description: 'description default',
  },
} as Meta

export const Basic: Story = (args) => <Main {...args} />
Basic.args = {
  title: 'title basic sadsadssdasdasdasd',
  description: 'description basic',
}

export const Secondary: Story = (args) => <Main {...args} />
Secondary.args = {
  title: 'title basic sadsadssdasdasdasd',
  description: 'description basic',
}

export const Default: Story = (args) => <Main {...args} />
