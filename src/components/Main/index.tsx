import * as S from './styles'

const Main = ({
  title = 'React Avançado',
  description = 'TypeSCript, ReactJS, NextJS e Styled Components',
}) => (
  <S.Wrapper>
    <S.Logo src="/img/logo.svg" alt="React Avançado" />
    <S.Title>{title}</S.Title>
    <S.Description>{description}</S.Description>
    <S.Illustration
      src="/img/hero-illustration.svg"
      alt="Desenvolvedor com código"
    />
  </S.Wrapper>
)
export default Main
